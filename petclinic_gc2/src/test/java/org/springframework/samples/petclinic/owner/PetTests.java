package org.springframework.samples.petclinic.owner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PetTests {
	private Pet mascota;
	private Owner dueño;
	/////////////////////////////////////////////////////////////////////
	@Before
	public void init() {
		
		if (this.mascota==null) {
			
			mascota = new Pet();
			
			mascota.setName("nombre");
			
			LocalDate fechaA = LocalDate.of(2005, 2, 1);
			mascota.setBirthDate(fechaA);
			
			
			
			if (this.mascota==null) {
				dueño = new Owner();
				dueño.setFirstName("Geormig");
				dueño.setLastName("Frankmig");
				dueño.setAddress("110 W. Libert");
				dueño.setCity("Madison");
				dueño.setTelephone("608678900");
			}
			
			mascota.setOwner(dueño);
			

		    //Añado peso
		    mascota.setWeight(2);
		    //Añado comentario
		    mascota.setComments("Comentario");
		}
		 
	}
	/////////////////////////////////////////////////////////////////////
	
	
	@Test
	public void testGetWeight() {
		assertNotNull(mascota.getWeight());
		assertFalse(mascota.getWeight()==1);
		assertTrue(mascota.getWeight()==2);
		
	}
	
	@Test
	public void testSetWeight() {
		assertNotNull(mascota);
		mascota.setWeight(1);
		assertFalse(mascota.getWeight()==2);
		assertTrue(mascota.getWeight()==1);
		
	}
	
	@Test
	public void testGetComments() {
		assertNotNull(mascota.getComments());
		assertFalse(mascota.getComments()=="Oruga");
		assertTrue(mascota.getComments()=="Comentario");
		
	}
	
	@Test
	public void testSetComments() {
		assertNotNull(mascota);
		mascota.setComments("Morirá en breve");;
		assertFalse(mascota.getComments()=="Vivo");
		assertTrue(mascota.getComments()=="Morirá en breve");
		
	}
	
	@After
	public void finish() {
		this.mascota=null;
		this.dueño=null;
	}
}


	


