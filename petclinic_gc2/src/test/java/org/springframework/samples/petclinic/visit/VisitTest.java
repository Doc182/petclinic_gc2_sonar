package org.springframework.samples.petclinic.visit;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.samples.petclinic.vet.Vet;

public class VisitTest {
	
	@Autowired
	public static Visit visit;
	public static Vet vet;

	@Before
	public void initClass() {
		if(this.visit==null) {
			
			visit = new Visit();
			LocalDate date = LocalDate.of(2018, 12, 14);
			visit.setDate(date);
			visit.setDescription("Probando JUnit Visit SrAzul");
			
			if(vet==null) {
				vet = new Vet();
				vet.setFirstName("Jesus");
				vet.setLastName("Tovar");
				vet.setHomeVisits(false);
			}
			visit.setVet(vet);
			
		}
	}
	
	@Test
	public void SetVetTest() {
		
		Vet vet1 = new Vet();
		vet1.setFirstName("Carlos");
		vet1.setLastName("Guillen");
		vet1.setHomeVisits(false);
		
		visit.setVet(vet1);
		
		assertTrue(visit.getVet().getFirstName()=="Carlos");
		assertTrue(visit.getVet().getLastName()=="Guillen");
		assertTrue(visit.getVet().getHomeVisits()==false);
		
		

	}
	
	@Test
	public void GetVetTest() {

		Vet vet1 = visit.getVet();
		
		assertNotNull(vet1.getFirstName());
		assertEquals(vet1.getFirstName(), vet.getFirstName());
		
		assertNotNull(vet1.getLastName());
		assertEquals(vet1.getLastName(), vet.getLastName());
		
		assertEquals(vet1.getHomeVisits(), vet.getHomeVisits());
		
	}
	
    @After
    public void finish() {
    	this.visit = null;
    	this.vet = null;
    }

}
