/*
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.vet;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.samples.petclinic.visit.Visit;
import org.springframework.util.SerializationUtils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.Month;
import java.util.Collection;
import java.util.List;

/**
 * @author Dave Syer
 *
 */
public class VetTests {
	
	public static Vet vet;
	
	@Before
    public void init() {
    	if(vet == null) {
        	vet = new Vet();
        	vet.setFirstName("Jesus");
        	vet.setLastName("Tovar");
        	vet.setHomeVisits(false);
    	}
    }
	
	@Test
	public void testHomeVisits() {
		assertNotNull(vet.getHomeVisits());
		assertFalse(vet.getHomeVisits());
	}
	
    @Test
    public void testSerialization() {
        Vet vet = new Vet();
        vet.setFirstName("Zaphod");
        vet.setLastName("Beeblebrox");
        vet.setId(123);
        Vet other = (Vet) SerializationUtils
                .deserialize(SerializationUtils.serialize(vet));
        assertThat(other.getFirstName()).isEqualTo(vet.getFirstName());
        assertThat(other.getLastName()).isEqualTo(vet.getLastName());
        assertThat(other.getId()).isEqualTo(vet.getId());
    }
    
    @Test
    public void testGetVisits() {
    	
    	List<Visit>getVisits = vet.getVisits();
    	
    	if(getVisits.size() > 0) {
    		assertArrayEquals(getVisits.toArray(),vet.getVisits().toArray());
    	}
    	assertTrue(vet.getVisits().size()==0);
    	
    }
    
    @Test 
    public void testAddVisits() {
    	
    	assertTrue(vet.getVisits().size()==0);
    	
    	LocalDate date = LocalDate.of(2018, 12, 14);
    	
    	Visit visit = new Visit();
    	visit.setVet(vet);
    	visit.setDate(date);
    	visit.setDescription("Probando JUnit Vet Sr.Azul");
    	vet.addVisit(visit);
    	
    	assertTrue(vet.getVisits().size()==1); 
    	
    }
	
    @After
    public void finish() {
    	vet = null;
    }

}
